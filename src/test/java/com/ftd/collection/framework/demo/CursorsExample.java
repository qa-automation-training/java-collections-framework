package com.ftd.collection.framework.demo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.testng.annotations.Test;

public class CursorsExample {
	
	@Test
	public void iteratorExample() {
		ArrayList<Integer> al = new ArrayList<Integer>();
		for(int i=1;i<=10;i++) {
			al.add(i);
		}
		System.out.println("Elements in ArrayList:"+al);
		
		Iterator<Integer> itr = al.iterator();
		while(itr.hasNext()) {
			if(!(itr.next()%2==0))
				itr.remove();				
		}
		System.out.println("ArrayList after iteration operation:"+al);
	}
	
	@Test
	public void listIteratorExamle() {
		List<String> names = new ArrayList<String>();
	    names.add("Shyam");
	    names.add("Rajat");
	    names.add("Paul");
	    names.add("Tom");
	    names.add("Kate");
	    
	    ListIterator<String> litr = names.listIterator();
	    System.out.println("Print the elements in forward direction:");
	    while(litr.hasNext()) {
	    	System.out.print(litr.next()+"--");
	    }
	    System.out.println("Print the elements in backward direction:");
	    while(litr.hasPrevious()) {
	    	System.out.print(litr.previous()+"--");
	    }
	}

}
