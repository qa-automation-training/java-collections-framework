package com.ftd.collection.framework.demo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.testng.annotations.Test;

public class MapImlementedClasses {
	
	@Test
	public void hashMapExample() {
		Map<Integer,String> map=new HashMap<Integer,String>();  
		  map.put(100,"Amit");  
		  map.put(110,"Vijay");  
		  map.put(102,"Rahul");
		  map.put(100, "Ravi");
		  map.putIfAbsent(102, "Rahul");
		  System.out.println(map);	//Order is not preserved
		  System.out.println(map.containsKey(100));
		  System.out.println(map.containsValue("Ramu"));
		  System.out.println(map.get(100));
		  
		  Set<Integer> s = map.keySet();
		  System.out.println("Set of keys:"+s);
		  
		  Collection<String> c = map.values();
		  System.out.println("List of values:"+c);
		  
		  Set<Entry<Integer,String>> eSet = map.entrySet();
		  System.out.println("Entries in map:"+eSet);
		  
		  Iterator<Entry<Integer, String>> itr = eSet.iterator();
		  while(itr.hasNext()){
			  Map.Entry<Integer,String> mE = itr.next();
			  System.out.println(mE.getKey()+" "+mE.getValue());
		  }
		  
		  //Synchronized version of Map
		  //Map<Integer, String> synMap = Collections.synchronizedMap(map);
	}
	
	@Test
	public void linkedHashMapExample() {
		Map<Integer, String> lmap = new LinkedHashMap<Integer, String>();
		lmap.put(100, "Amit");
		lmap.put(110, "Vijay");
		lmap.put(102, "Rahul");
		lmap.put(100, "Ravi");
		System.out.println("Elements in LinkedHashMap:"+lmap);

	}

}
