package com.ftd.collection.framework.demo;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.testng.annotations.Test;

public class SetInterfaceImplementedClasses {
	
	@Test
	public void hashSetExample() {
		HashSet<String> h = new HashSet<String>();
		h.add("Z");
		h.add("A");
		h.add("A");
		h.add("D");
		h.add("B");
		System.out.println("Elements in HashSet:"+h);
	}
	
	@Test void linkedHashSetExample() {
		Set<String> lh = new LinkedHashSet<String>();
		lh.add("Z");
		lh.add("A");
		lh.add("A");
		lh.add("D");
		lh.add("B");
		System.out.println("Elements in Linked Hash Set:"+lh);
	}

}
