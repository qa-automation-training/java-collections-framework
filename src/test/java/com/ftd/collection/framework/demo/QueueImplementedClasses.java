package com.ftd.collection.framework.demo;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

import org.testng.annotations.Test;

public class QueueImplementedClasses {

	@Test
	public void linkedListExample() {
		Queue<String> q = new LinkedList<String>();
		q.add("Z"); // return true upon success (for limited size queue)
		q.offer("A");
		q.offer("Y");
		q.add("B");
		System.out.println("Elements in Queue:" + q);
		System.out.println("Head of the queue:" + q.peek());
		q.poll();
		q.remove();
		System.out.println("Elements in queue:" + q);
	}

	@Test
	public void prioprityQueueExample() {
		Queue<Integer> q = new PriorityQueue<Integer>();
		q.add(10);
		q.add(5);
		q.add(20);
		System.out.println(q);
	}

	class Book implements Comparable<Book> {
		int id;
		String name, author, publisher;
		int quantity;

		public Book(int id, String name, String author, String publisher, int quantity) {
			this.id = id;
			this.name = name;
			this.author = author;
			this.publisher = publisher;
			this.quantity = quantity;
		}

		public int compareTo(Book b) {
			if (id < b.id) {
				return 1;
			} else if (id > b.id) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	@Test
	public void priorityQueueImplementSComparable() {
		Queue<Book> queue = new PriorityQueue<Book>();

		Book b1 = new Book(121, "Let us C", "Yashwant Kanetkar", "BPB", 8);
		Book b2 = new Book(233, "Operating System", "Galvin", "Wiley", 6);
		Book b3 = new Book(101, "Data Communications & Networking", "Forouzan", "Mc Graw Hill", 4);

		queue.add(b1);
		queue.add(b2);
		queue.add(b3);
		System.out.println("Traversing the queue elements:");

		for (Book b : queue) {
			System.out.println(b.id + " " + b.name + " " + b.author + " " + b.publisher + " " + b.quantity);
		}

	}

	@Test
	public void priorityBlockingQueue() {

		Queue<Integer> pbq = new PriorityBlockingQueue<Integer>();//provides synchronization
		pbq.add(10);
		pbq.add(20);
		pbq.add(15);
		System.out.println(pbq.peek());
		System.out.println(pbq.poll());
		System.out.println(pbq.peek());
	}

	@Test
	public void arrayDqueueExample() {
		Deque<String> deque = new ArrayDeque<String>();
		deque.add("B");
		deque.add("D");
		deque.add("C");
		System.out.println("Elements in deque:" + deque);
		deque.addFirst("A");
		deque.addLast("Z");
		System.out.println("Elements in deque:" + deque);
		deque.pollFirst();
		deque.pollLast();
		System.out.println("Elements in deque:" + deque);
	}
}
