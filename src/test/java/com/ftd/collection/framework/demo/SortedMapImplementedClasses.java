package com.ftd.collection.framework.demo;

import java.util.Comparator;
import java.util.TreeMap;

import org.testng.annotations.Test;

public class SortedMapImplementedClasses {

	@Test
	public void treeMapExample() {

		TreeMap<Integer, String> t = new TreeMap<Integer, String>();
		t.put(100, "A");
		t.put(120, "C");
		t.put(110, "B");
		System.out.println("Key value pairs in map:" + t);
	}

	class MyComparator implements Comparator<String> {

		public int compare(String s1, String s2) {
			return s1.compareTo(s2);
		}

	}

	@Test
	public void treeMapWithCustomOrder() {
		TreeMap<String, Integer> t = new TreeMap<String, Integer>(new MyComparator());
		t.put("Y", 1);
		t.put("A", 2);
		t.put("Z", 3);
		t.put("M", 4);
		System.out.println(t);
	}
	

}
