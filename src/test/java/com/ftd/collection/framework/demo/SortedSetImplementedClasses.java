package com.ftd.collection.framework.demo;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

import org.testng.annotations.Test;

public class SortedSetImplementedClasses {

	@Test
	public void comparableExample() {
		System.out.println("Z".compareTo("N"));
		System.out.println("A".compareTo("N"));
		System.out.println("B".compareTo("N"));
		// System.out.println("D".compareTo(null));//NPE
	}

	@Test
	public void treeSetExample1() {
		TreeSet<String> t = new TreeSet<String>();
		t.add("N");
		t.add("Z");// N.comapareTo("Z");
		t.add("A");
		t.add("B");
		System.out.println("Elements in tree:" + t);
	}
	
	

	@Test
	public void testSetExample2() {
		TreeSet<StringBuffer> t = new TreeSet<StringBuffer>();
		t.add(new StringBuffer("A"));
		t.add(new StringBuffer("Z"));
		t.add(new StringBuffer("L"));
		t.add(new StringBuffer("B"));
		System.out.println(t);
	}

	class MyComparator implements Comparator<StringBuffer> {

		public int compare(StringBuffer sb1, StringBuffer sb2) {
			String s1 = sb1.toString();
			String s2 = sb2.toString();
			return -s1.compareTo(s2);// s2 is first inserted object,s1 is currently inserted object
			// return s2.compareTo(s1);
		}
	}

	@Test
	public void treeSetExample3() {
		TreeSet<StringBuffer> t = new TreeSet<StringBuffer>(new MyComparator());
		t.add(new StringBuffer("A"));
		t.add(new StringBuffer("Z"));
		t.add(new StringBuffer("L"));
		t.add(new StringBuffer("B"));
		System.out.println("Elements in Treeset:" + t);
	}

	
	//For user defined classes, default natural sorting order needs to be defined.
	class Employee implements Comparable<Employee> {
		String name;
		int empId;

		Employee(String name, int empId) {
			this.name = name;
			this.empId = empId;
		}

		public String toString() {
			return name + "--" + empId;
		}

		public int compareTo(Employee e) {
			int eid1 = this.empId;
			int eid2 = e.empId;
			if (eid1 < eid2)
				return -1;
			else if (eid1 > eid2)
				return +1;
			else
				return 0;
		}
	}

	@Test
	public void treeSetWithDefaultSorting() {
		Employee e1 = new Employee("A", 100);
		Employee e2 = new Employee("Z", 50);
		Employee e3 = new Employee("B", 200);
		Employee e4 = new Employee("Y", 150);

		TreeSet<Employee> t = new TreeSet<Employee>();
		t.add(e1);
		t.add(e2);
		t.add(e3);
		t.add(e4);
		System.out.println("Employees in TreeSet:" + t);
	}
	
	//User defined sorting
	class MyComp implements Comparator<Employee> {
		public int compare(Employee e1, Employee e2) {
			String s1 = e1.name;
			String s2 = e2.name;
			return s1.compareTo(s2);
		}
	}

	@Test
	public void treeSetWithCustomSorting() {
		Employee e1 = new Employee("A", 100);
		Employee e2 = new Employee("Z", 50);
		Employee e3 = new Employee("B", 200);
		Employee e4 = new Employee("Y", 150);

		TreeSet<Employee> t = new TreeSet<Employee>(new MyComp());//Input comparator object
		t.add(e1);
		t.add(e2);
		t.add(e3);
		t.add(e4);
		System.out.println("Employees in TreeSet:" + t);
	}
	
	
	
	
	@Test
	public void setTimeComplexity() {

		HashSet<Integer> hs = new HashSet<Integer>();
		LinkedHashSet<Integer> lhs = new LinkedHashSet<Integer>();
		TreeSet<Integer> ts = new TreeSet<Integer>();

		for (int i = 0; i < 100000; i++) {
			hs.add(i);
			lhs.add(i);
			ts.add(i);
		}

		// Searching in HashSet
		long startTime = System.nanoTime();
		for (int j = 10000; j < 11000; j++) {
			hs.contains(j);
		}
		long endTime = System.nanoTime();
		long duration = endTime - startTime;
		System.out.println("HashSet: " + duration);

		// Searching in LinkedHashSet
		long startTime1 = System.nanoTime();
		for (int k = 10000; k < 11000; k++) {
			lhs.contains(k);
		}
		long endTime1 = System.nanoTime();
		long duration1 = endTime1 - startTime1;
		System.out.println("LinkedHashSet: " + duration1);

		// Searching in TreeSet
		long startTime2 = System.nanoTime();
		for (int l = 10000; l < 11000; l++) {
			ts.contains(l);
		}
		long endTime2 = System.nanoTime();
		long duration2 = endTime2 - startTime2;
		System.out.println("TreeSet: " + duration2);

	}

	
}
