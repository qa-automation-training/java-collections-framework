package com.ftd.collection.framework.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.testng.annotations.Test;

public class ListImplementedClasses {
	
	 @Test
	public void arrayListExample() {
		ArrayList<String> al = new ArrayList<String>();
		al.add("A");
		al.add("Z");
		al.add("B");
		al.add("Y");
		al.add("A");
		System.out.println("Array List Elemets:" + al);
		al.add(0, "S");
		al.set(0, "T");
		System.out.println("Array List Elemets:" + al);
	}

	 @Test
	public void linkedListExample() {
		LinkedList<String> l = new LinkedList<String>();
		l.add("A");
		l.add("Z");
		l.add("B");
		l.add("Y");
		l.add("A");
		System.out.println("Linked List Elemets:" + l);
	}

	 @Test
	public void syncArrayList() {
		List<String> al = new ArrayList<String>();
		al = Collections.synchronizedList(al);
	}

	
	 @Test
	 public void additionTimeComplexity() {
		 ArrayList<Integer> arrayList = new ArrayList<Integer>();
		 LinkedList<Integer> linkedList = new LinkedList<Integer>();
		  
		 // ArrayList add
		 long startTime = System.nanoTime();
		 for (int i = 0; i < 100000; i++) {
		 	arrayList.add(i);
		 }
		 long endTime = System.nanoTime();
		 long duration = endTime - startTime;
		 System.out.println("ArrayList add:  " + duration);
		  
		 // LinkedList add
		 startTime = System.nanoTime();
		  
		 for (int i = 0; i < 100000; i++) {
		 	linkedList.add(i);
		 }
		 endTime = System.nanoTime();
		 duration = endTime - startTime;
		 System.out.println("LinkedList add: " + duration);
		  
	 }
	 
	 
	 @Test
	 public void retrievalTimeComplexity() {
		 ArrayList<Integer> arrayList = new ArrayList<Integer>();
		 LinkedList<Integer> linkedList = new LinkedList<Integer>();
		  
		 // ArrayList add
		 for (int i = 0; i < 100000; i++) {
		 	arrayList.add(i);
		 }

		 // LinkedList add
		 for (int i = 0; i < 100000; i++) {
		 	linkedList.add(i);
		 }
		  
		 // ArrayList get
		 long startTime = System.nanoTime();
		  
		 for (int i = 0; i < 10000; i++) {
		 	arrayList.get(i);
		 }
		 long endTime = System.nanoTime();
		 long duration = endTime - startTime;
		 System.out.println("ArrayList get:  " + duration);
		  
		 // LinkedList get
		 startTime = System.nanoTime();
		  
		 for (int i = 0; i < 10000; i++) {
		 	linkedList.get(i);
		 }
		 endTime = System.nanoTime();
		 duration = endTime - startTime;
		 System.out.println("LinkedList get: " + duration);
		  
	 }
	 
	 @Test
	 public void removalTimeComplexity() {
		 ArrayList<Integer> arrayList = new ArrayList<Integer>();
		 LinkedList<Integer> linkedList = new LinkedList<Integer>();
		  
		 // ArrayList add
		 for (int i = 0; i < 100000; i++) {
		 	arrayList.add(i);
		 }

		 // LinkedList add
		 for (int i = 0; i < 100000; i++) {
		 	linkedList.add(i);
		 }
		 
		// ArrayList remove
		 long startTime = System.nanoTime();
		  
		 for (int i = 9999; i >=0; i--) {
		 	arrayList.remove(i);
		 }
		 long endTime = System.nanoTime();
		 long duration = endTime - startTime;
		 System.out.println("ArrayList remove:  " + duration);
		  
		  
		  
		 // LinkedList remove
		 startTime = System.nanoTime();
		  
		 for (int i = 9999; i >=0; i--) {
		 	linkedList.remove(i);
		 }
		 endTime = System.nanoTime();
		 duration = endTime - startTime;
		 System.out.println("LinkedList remove: " + duration);

		 
	 }
	 
	  
}
